#include <iostream>

void PrintNumber(int N, bool Even)
{
    for (int a = 0; a < N; a++)
    {
        if (Even)
        {
            std::cout << ++a << " - Odd number \n";
        }
        else
        {
            std::cout << a++ << " - Even number \n";
        }
    }
}

int main()
{
    PrintNumber(100, true);
    PrintNumber(100, false);
}
